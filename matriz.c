#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <getopt.h>
#include <unistd.h>
#include <pthread.h>

struct Argumentos{
	double **m1;
	unsigned long m1f;
	unsigned long m1c;
	double **m2;
	unsigned long m2f;
	unsigned long m2c;
	double **res;
	unsigned long resf;
	unsigned long resc;
	int id;
	int hilos;
};

//unsigned long HILO[8]={0,0,0,0,0,0,0,0};

double **crear_matriz(unsigned long filas, unsigned long columnas){

	double **matriz = (double **)calloc(filas, sizeof(double *));

	if(matriz == NULL){
		return NULL;	
	}

	unsigned long i = 0;
	for(i = 0; i < filas; i++){
		matriz[i] = calloc(columnas, sizeof(double));	
	}

	return matriz;
}


void liberar_matriz(double **m, unsigned long filas, unsigned long columnas){
	for(unsigned long i = 0; i < filas; i++){
		free(m[i]);	
	}
	free(m);
}


void llenar_matriz_azar(double **m, unsigned long filas, unsigned long columnas, float max){
	srand( time( NULL ) );
	for(unsigned long i = 0; i < filas; i++){
		for(unsigned long j = 0; j < columnas; j++){
			m[i][j] = ((double)rand()/(double)(RAND_MAX)) * max;
		}	
	}

}

void mostrar_matriz(double **m, unsigned long filas, unsigned long columnas){
	
	for(unsigned long i = 0; i < filas; i++){
		for(unsigned long j = 0; j < columnas; j++){
			printf("%6.2f ", m[i][j]); 
		}
		printf("\n");	
	}
	printf("\n");

}


//Calcula el producto punto de una fila de m1, con una columna de m2
double producto_punto(double **m1, unsigned long filasM1, unsigned long columnasM1, unsigned long fila,
	             double **m2, unsigned long filasM2, unsigned long columnasM2, unsigned long columna){

	double resultado = 0.0f;
	for(unsigned long j = 0; j < columnasM1; j++){
		for(unsigned long i = 0; i < filasM2; i++){
			resultado += (m1[fila][j] * m2[i][columna]);
			j++;
		}	
	}
	return resultado;
}

void *multiplicar_matrices(void *arg){
	struct Argumentos *argumento=(struct Argumentos *)arg;
	
	int i=0;
	int id=argumento->id;
	int n=argumento->hilos;
	
	//HILO[id]=pthread_self();

	for(unsigned long filres = id; filres < (argumento->resf); filres=id+(i*n)){
		for(unsigned long colres = 0; colres < (argumento->resc); colres++){
			(argumento->res)[filres][colres] = producto_punto(argumento->m1, argumento->m1f, argumento->m1c, filres,
							     argumento->m2, argumento->m2f, argumento->m2c, colres);
		}
		i++;
	}
	
	return 0;

}


void crear_hilo(double **m1, unsigned long filasM1, unsigned long columnasM1,
			  double **m2, unsigned long filasM2, unsigned long columnasM2,
			  double **res, unsigned long filasRes, unsigned long columnasRes,
			  int hilos){

	struct Argumentos argumento;
	argumento.m1=m1;
	argumento.m1f=filasM1;
	argumento.m1c=columnasM1;
	argumento.m2=m2;
	argumento.m2f=filasM2;
	argumento.m2c=columnasM2;
	argumento.res=res;
	argumento.resf=filasRes;
	argumento.resc=columnasRes;
	argumento.hilos=hilos;
	
	pthread_t idHilo;
	for(int i=0;i<hilos;i++){
		argumento.id=i;
		pthread_create(&idHilo, NULL, multiplicar_matrices, &argumento);
		pthread_join(idHilo,NULL);		
	}

}


double obtener_tiempo(){
	struct timespec tsp;
	
	clock_gettime(CLOCK_REALTIME, &tsp);
	double secs = (double)tsp.tv_sec;
	double nsecs = (double)tsp.tv_nsec / 1000000000.0f;

	return secs + nsecs;

}

int main(int argc, char **argv){

	unsigned long m1Filas = -1;
	unsigned long m1Cols = -1;

	unsigned long m2Filas = -1;
	unsigned long m2Cols = -1;
	int nHilos = -1;
	
	static struct option long_options[7] = {
		{"filas_m1",  required_argument, 0, 'a' },
		{"cols_m1",   required_argument, 0, 'b' },
		{"filas_m2",  required_argument, 0, 'c' },
		{"cols_m2",   required_argument, 0, 'd' },
		{"n_hilos",   required_argument, 0, 'e' },
		{"mostrar_matrices", no_argument  , 0, 'f' },
		{0, 0, 0, 0 }
	    };



	char opcion = 0;
	int mostra_matrices = 0;
	int long_index = 0;
	while ( (opcion = getopt_long(argc,argv,  "a:b:c:d:e:f:", long_options,  &long_index)) != -1){
		switch(opcion){
			case 'a':
				m1Filas = atol(optarg);
				break;
			case 'b':
				m1Cols = atol(optarg);
				break;
			case 'c':
				m2Filas = atol(optarg);
				break;
			case 'd':
				m2Cols = atol(optarg);
				break;
			case 'e':
				nHilos = atoi(optarg);
				break;
			case 'f':
				mostra_matrices = 1;
				break;
			default:
				break;
		}
	}

	if(m1Cols != m2Filas){
		printf("El numero de columnas de la matriz m1 debe ser igual al numero de filas de la matriz m2\n");
		exit(-1);	
	}

	if(nHilos == 0 || nHilos == -1){
		printf("Numero de hilos invalido\n");
		return -1;	
	}



	unsigned long resFilas = m1Filas;
	unsigned long resCols = m2Cols;

	double **m1 = crear_matriz(m1Filas,m1Cols);
	double **m2 = crear_matriz(m2Filas, m2Cols);

	
	llenar_matriz_azar(m1, m1Filas, m1Cols, 100.0f);
	sleep(2);
	llenar_matriz_azar(m2, m2Filas, m2Cols, 100.0f);

	if(mostra_matrices){
		printf("Matriz m1:\n");
		mostrar_matriz(m1,m1Filas, m1Cols);
	}

	if(mostra_matrices){
		printf("Matriz m2:\n");
		mostrar_matriz(m2,m2Filas, m2Cols);
	}
	
	double **resultado = crear_matriz(resFilas, resCols);
	
	double ini = obtener_tiempo();
	crear_hilo(m1, m1Filas, m1Cols, m2, m2Filas, m2Cols, resultado, resFilas, resCols, nHilos);	
	
	/*
	for(int j=0;j<8;j++){
		if(HILO[j]!=0){
			pthread_join(HILO[j],NULL);
		}
	}
	*/
	
	double fin = obtener_tiempo();
	double delta = fin - ini;

	
	
	if(mostra_matrices){
		mostrar_matriz(resultado,resFilas, resCols);	
	}
	printf("\nTiempo de multiplicacion de matrices %.6f\n", delta);


	liberar_matriz(m1, m1Filas, m1Cols);
	liberar_matriz(m2, m2Filas, m2Cols);
	liberar_matriz(resultado, resFilas, resCols);

}


